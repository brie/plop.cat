---
marp: true
---
<!--
_backgroundColor: black
paginate: true
footer: '🐱‍💻 Brie Carranza 🐱‍🚀'
-->


# 😻 Cats I have loved! 😻

  - 🌈🌉 Across the rainbow bridge! 🌉🌈
  - Prinkles
  - Assorted Cats
  - Plop

---
<!--
_backgroundColor: black
paginate: true
footer: '🐱‍💻 Brie Carranza 🐱‍🚀'
-->

## 🌈🌉 Across the rainbow bridge! 🌉🌈

  - [ ] TODO add a collage

---

## Prinkles



---

## Assorted Cats

  - Boo Radley


---

# Plop Beauregard

Plop is my cat. I love him _very_ much. 

![](photos/plop-sits-in-box.jpg)


---

# How Plop joined us!

## Girl meets cat


## Cat plops


## Girl loves Plop

---

## Plop comes home


### I love Plop. 

---


# A painting of Plop

---

# Plop loves box



# Plop loves sink


# Plop tolerates camping


# Plop on leash


---

# Plop's door


# Plop on TV

---

# Plop works hard!

  - He knows a fancy trick!

---

# Plop and Brie

**FUR**ever

We have a matching bracelet/collar set for special occasions, Plop's birthday on July 4th. 


---

## On a dark and stormy night

May 10, 2020


## Why is Plop wet?


## Oh my God!


A possum!

---
<!--
_backgroundColor: green
paginate: true
footer: '🐱‍💻'
-->



# ...and meow!

...a special guest: **Plop**!
